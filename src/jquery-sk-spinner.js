
import { SkSpinnerImpl }  from '../../sk-spinner/src/impl/sk-spinner-impl.js';

export class JquerySkSpinner extends SkSpinnerImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'spinner';
    }

    get spinnerImagePath() {
        if (! this._spinnerImagePath) {
            this._spinnerImagePath = (this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path'))
                ? `/${this.comp.configEl.getAttribute('tpl-path')}/loading_spinner.gif`
                :`/node_modules/sk-theme-jquery/loading_spinner.gif`;
        }
        return this._spinnerImagePath;
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
        this.comp.el.querySelector('.spinner').style.backgroundImage = `url(${this.spinnerImagePath})`;
    }
}
